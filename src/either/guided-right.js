import {Right as _Right} from 'fantasy-eithers'
import {guided} from '@either/guided'

export const Right = _Right
export const GuidedRight = guided(Right)
